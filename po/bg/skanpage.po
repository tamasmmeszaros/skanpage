# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the skanpage package.
#
# SPDX-FileCopyrightText: 2022, 2023 Mincho Kondarev <mkondarev@yahoo.de>
msgid ""
msgstr ""
"Project-Id-Version: skanpage\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-28 00:42+0000\n"
"PO-Revision-Date: 2023-11-13 13:28+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Минчо Кондарев"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mkondarev@yahoo.de"

#: DocumentModel.cpp:52 DocumentModel.cpp:69 DocumentModel.cpp:80
#, kde-format
msgid "New document"
msgstr "Нов документ"

#: DocumentModel.cpp:72
#, kde-format
msgctxt "for file names, indicates a range: from file0000.png to file0014.png"
msgid "%1 ... %2"
msgstr "%1 ... %2"

#: DocumentPrinter.cpp:33
#, kde-format
msgid "Nothing to print."
msgstr "Няма нищо за отпечатване."

#: DocumentPrinter.cpp:37
#, kde-format
msgid "Previous printing operation still in progress."
msgstr "Предишната операция по отпечатване все още се изпълнява."

#: DocumentPrinter.cpp:69
#, kde-format
msgid "Document sent to printer."
msgstr "Документът е изпратен до принтера."

#: DocumentSaver.cpp:34
#, kde-format
msgid "No file path given."
msgstr "Не е посочен път до файла."

#: DocumentSaver.cpp:38
#, kde-format
msgid "Nothing to save."
msgstr "Няма нищо за запазване."

#: DocumentSaver.cpp:42
#, kde-format
msgid "Saving to non-local directories is currently unsupported."
msgstr "Записването в нелокални директории понастоящем не се поддържа."

#: DocumentSaver.cpp:67 DocumentSaver.cpp:98
#, kde-format
msgctxt "%1 is the error message"
msgid "An error ocurred while saving: %1."
msgstr "Грешка при запис: %1."

#: DocumentSaver.cpp:79
#, kde-format
msgid "Document saved as PDF."
msgstr "Документът  е записан като PDF."

#: DocumentSaver.cpp:111
#, kde-format
msgid "Document saved with OCR as PDF."
msgstr "Документът е запазен като PDF след оптично разпознаване."

#: DocumentSaver.cpp:183
#, kde-format
msgid "Document saved as image."
msgstr "Документът  е записан като изображение."

#: DocumentSaver.cpp:192
#, kde-format
msgid "Failed to save document as image."
msgstr "Неуспех при запазването на документа като изображение."

#: DocumentSaver.cpp:207
#, kde-format
msgid "Failed to save image"
msgstr "Неуспех при запазване на изображението"

#: main.cpp:56
#, kde-format
msgid "Skanpage"
msgstr "Skanpage"

#: main.cpp:58
#, kde-format
msgid "Multi-page scanning application by KDE."
msgstr "Приложение на КДЕ за сканиране на много страници."

#: main.cpp:60
#, kde-format
msgid "© 2015-2022 Kåre Särs, Alexander Stippich"
msgstr "© 2015-2022 Kåre Särs, Alexander Stippich"

#: main.cpp:63 main.cpp:64
#, kde-format
msgid "developer"
msgstr "разработчик"

#: main.cpp:75
#, kde-format
msgid "Sane scanner device name. Use 'test' for test device."
msgstr ""
"Име на Sane скенер устройство. Използвайте 'test' за тестване на "
"устройството."

#: main.cpp:75
#, kde-format
msgid "device"
msgstr "устройство"

#: qml/ContentView.qml:18
#, kde-format
msgctxt "Prefix for document name indicating an unsaved document"
msgid "* %1"
msgstr "* %1"

#: qml/DeviceSelection.qml:54
#, kde-format
msgctxt ""
"Device vendor with device model, followed by the device name identifier"
msgid ""
"%1 %2\n"
"(%3)"
msgstr ""
"%1 %2\n"
"(%3)"

#: qml/DeviceSelection.qml:69
#, kde-kuit-format
msgctxt "@info"
msgid "No devices found."
msgstr "Няма намерени устройства."

#: qml/DeviceSelection.qml:86
#, kde-format
msgid "Open selected device"
msgstr "Отваряне на избраното устройство"

#: qml/DeviceSelection.qml:95
#, kde-format
msgid "Reload devices list"
msgstr "Презареждане списъка с устройства"

#: qml/DevicesLoading.qml:29
#, kde-kuit-format
msgctxt "@info"
msgid "Searching for available devices."
msgstr "Търсене на налични устройства."

#: qml/DocumentList.qml:72
#, kde-format
msgid "Select previous page"
msgstr "Избиране на предишната страница"

#: qml/DocumentList.qml:78
#, kde-format
msgid "Select next page"
msgstr "Избиране на следващата страница"

#: qml/DocumentList.qml:84
#, kde-format
msgid "Move selected page up"
msgstr "Преместване нагоре на избраната страница"

#: qml/DocumentList.qml:93
#, kde-format
msgid "Move selected page down"
msgstr "Преместване надолу на избраната страница"

#: qml/DocumentList.qml:205
#, kde-kuit-format
msgctxt "@info"
msgid "Processing page..."
msgstr "Обработване на страницата..."

#: qml/DocumentList.qml:218
#, kde-format
msgctxt "Page index"
msgid "Page %1"
msgstr "Страница %1"

#: qml/DocumentList.qml:229
#, kde-format
msgid "Move Up"
msgstr "Нагоре"

#: qml/DocumentList.qml:239
#, kde-format
msgid "Move Down"
msgstr "Надолу"

#: qml/DocumentList.qml:249 qml/DocumentPage.qml:418
#, kde-format
msgid "Rotate Left"
msgstr "Завъртване наляво"

#: qml/DocumentList.qml:255 qml/DocumentPage.qml:425
#, kde-format
msgid "Rotate Right"
msgstr "Завъртване надясно"

#: qml/DocumentList.qml:261 qml/DocumentPage.qml:432
#, kde-format
msgid "Flip"
msgstr "Обръщане"

#: qml/DocumentList.qml:267 qml/DocumentPage.qml:439
#, kde-format
msgid "Save Page"
msgstr "Запазване на страницата"

#: qml/DocumentList.qml:273 qml/DocumentPage.qml:446
#, kde-format
msgid "Delete Page"
msgstr "Изтриване на страница"

#: qml/DocumentList.qml:292
#, kde-format
msgid "%1 page"
msgid_plural "%1 pages"
msgstr[0] "%1 страница"
msgstr[1] "%1 страници"

#: qml/DocumentList.qml:301
#, kde-format
msgid "Reorder Pages"
msgstr "Пренареждане на страниците"

#: qml/DocumentList.qml:306
#, kde-format
msgctxt "Indicates how pages are going to be reordered"
msgid "13 24 → 1234"
msgstr "13 24 → 1234"

#: qml/DocumentList.qml:313
#, kde-format
msgctxt "Indicates how pages are going to be reordered"
msgid "13 42 → 1234"
msgstr "13 42 → 1234"

#: qml/DocumentList.qml:320
#, kde-format
msgid "Reverse Order"
msgstr "В обратен ред"

#: qml/DocumentPage.qml:56
#, kde-format
msgid "Show Preview"
msgstr "Показване на предварителен преглед"

#: qml/DocumentPage.qml:72
#, kde-kuit-format
msgctxt "@info"
msgid "You do not have any images in this document.<nl/><nl/>Start scanning!"
msgstr ""
"В този документ няма никакви изображения.<nl/><nl/>Започнете сканирането!"

#: qml/DocumentPage.qml:258
#, kde-format
msgid "Discard this selection"
msgstr "Отказване на тази селекция"

#: qml/DocumentPage.qml:278
#, kde-format
msgid "Add Selection Area"
msgstr "Добавяне на област на селекция"

#: qml/DocumentPage.qml:283
#, kde-format
msgid "Click and drag to select another area"
msgstr "Кликнете и завлечете с мишката, за да избере друга област"

#: qml/DocumentPage.qml:293
#, kde-format
msgid "Split Scan Vertically"
msgstr "Разделяне на сканирането вертикално"

#: qml/DocumentPage.qml:304
#, kde-format
msgid "Split Scan Horizontally"
msgstr "Разделяне на сканирането хоризонтално"

#: qml/DocumentPage.qml:376
#, kde-format
msgid "Zoom In"
msgstr "Увеличаване на мащаба"

#: qml/DocumentPage.qml:385
#, kde-format
msgid "Zoom Out"
msgstr "Намаляване на мащаба"

#: qml/DocumentPage.qml:394
#, kde-format
msgid "Zoom Fit"
msgstr "Оптимално мащабиране"

#: qml/DocumentPage.qml:410
#, kde-format
msgid "Zoom 100%"
msgstr "Мащабиране 100%"

#: qml/ExportWindow.qml:21 qml/MainWindow.qml:103
#, kde-format
msgid "Export PDF"
msgstr "Експортиране в PDF"

#: qml/ExportWindow.qml:44
#, kde-format
msgid "Title:"
msgstr "Заглавие:"

#: qml/ExportWindow.qml:60
#, kde-format
msgid "File:"
msgstr "Файл:"

#: qml/ExportWindow.qml:85
#, kde-format
msgid "Enable optical character recognition (OCR)"
msgstr "Активиране на оптично разпознаване на текст (OCR))"

#: qml/ExportWindow.qml:105
#, kde-format
msgid "Languages:"
msgstr "Езици:"

#: qml/ExportWindow.qml:135
#, kde-format
msgid "%1 [%2]"
msgstr "%1 [ %2]"

#: qml/ExportWindow.qml:156
#, kde-format
msgid ""
"If your required language is not listed, please install Tesseract's language "
"file with your package manager."
msgstr ""
"Ако желаният от вас език не е посочен в списъка, моля, инсталирайте езика "
"Tesseract чрез вашия мениджър на пакети."

#: qml/ExportWindow.qml:182
#, kde-format
msgid "Save"
msgstr "Запазване"

#: qml/ExportWindow.qml:198 qml/MainWindow.qml:136
#, kde-format
msgid "Cancel"
msgstr "Отказ"

#: qml/GlobalMenu.qml:26
#, kde-format
msgctxt "menu category"
msgid "File"
msgstr "Файл"

#: qml/GlobalMenu.qml:68
#, kde-format
msgctxt "menu category"
msgid "Scan"
msgstr "Сканиране"

#: qml/GlobalMenu.qml:124
#, kde-format
msgctxt "menu category"
msgid "Help"
msgstr "Помощ"

#: qml/InProgressPage.qml:26
#, kde-kuit-format
msgctxt "Countdown string with time given in seconds"
msgid "Next scan starts in<nl/>%1 s"
msgstr "Следващото сканиране започва след <nl/>%1 с"

#: qml/InProgressPage.qml:44
#, kde-format
msgctxt "@info"
msgid "Scan in progress."
msgstr "Сканирането се изпълнява."

#: qml/MainWindow.qml:23
#, kde-format
msgctxt "document title: app title"
msgid "%1 ― Skanpage"
msgstr "%1 ― Skanpage"

#: qml/MainWindow.qml:85
#, kde-format
msgid "Discard All"
msgstr "Отхвърляне на всичко"

#: qml/MainWindow.qml:94
#, kde-format
msgid "Save All"
msgstr "Запис на всички"

#: qml/MainWindow.qml:111
#, kde-format
msgid "Preview"
msgstr "Предварителен преглед"

#: qml/MainWindow.qml:123
#, kde-format
msgid "Scan"
msgstr "Сканиране"

#: qml/MainWindow.qml:145
#, kde-format
msgid "Show Scanner Options"
msgstr "Показване на настройки на скенер"

#: qml/MainWindow.qml:155 qml/ShareWindow.qml:20
#, kde-format
msgid "Share"
msgstr "Споделяне"

#: qml/MainWindow.qml:163
#, kde-format
msgid "Print"
msgstr "Отпечатване"

#: qml/MainWindow.qml:184 qml/MainWindow.qml:381
#, kde-format
msgid "About Skanpage"
msgstr "Относно Skanpage"

#: qml/MainWindow.qml:191
#, kde-format
msgid "Configure Skanpage…"
msgstr "Конфигуриране на Skanpage…"

#: qml/MainWindow.qml:199
#, kde-format
msgid "Configure Keyboard Shortcuts…"
msgstr "Конфигуриране на клавишни комбинации…"

#: qml/MainWindow.qml:207
#, kde-format
msgid "Quit"
msgstr "Изход"

#: qml/OptionDelegate.qml:55
#, kde-format
msgid "%1:"
msgstr "%1:"

#: qml/OptionDelegate.qml:150
#, kde-format
msgctxt "Add ':' to make a header text"
msgid "%1:"
msgstr "%1:"

#: qml/OptionDelegate.qml:154
#, kde-format
msgctxt "@option:check a noun, as in 'the default setting'"
msgid "Default"
msgstr "По подразбиране"

#: qml/OptionDelegate.qml:168
#, kde-format
msgctxt "%1 is a numeric value"
msgid "Brightness: %1"
msgstr "Яркост: %1"

#: qml/OptionDelegate.qml:181
#, kde-format
msgctxt "%1 is a numeric value"
msgid "Contrast: %1"
msgstr "Контраст: %1"

#: qml/OptionDelegate.qml:194
#, kde-format
msgctxt "%1 is a numeric value"
msgid "Gamma: %1"
msgstr "Гама: %1"

#: qml/OptionDelegate.qml:224
#, kde-format
msgctxt "Adding unit suffix"
msgid "%1 %2"
msgstr "%1 %2"

#: qml/OptionDelegate.qml:249
#, kde-format
msgctxt "Unit suffix for bit"
msgid "bit"
msgstr "bit"

#: qml/OptionDelegate.qml:252
#, kde-format
msgctxt "Unit suffix for DPI"
msgid "DPI"
msgstr "DPI"

#: qml/OptionDelegate.qml:255
#, kde-format
msgctxt "Unit suffix for microsecond"
msgid "µs"
msgstr "µs"

#: qml/OptionDelegate.qml:258
#, kde-format
msgctxt "Unit suffix for second"
msgid "s"
msgstr "с"

#: qml/OptionDelegate.qml:261
#, kde-format
msgctxt "Unit suffix for millimeter"
msgid "mm"
msgstr "mm"

#: qml/OptionDelegate.qml:264
#, kde-format
msgctxt "Unit suffix for percent"
msgid "%"
msgstr "%"

#: qml/OptionDelegate.qml:267
#, kde-format
msgctxt "Unit suffix for pixel"
msgid "px"
msgstr "px"

#: qml/OptionsPanel.qml:30
#, kde-format
msgid "Select options for quick access:"
msgstr "Опции за бърз достъп:"

#: qml/OptionsPanel.qml:68
#, kde-format
msgctxt "scanner device vendor and model"
msgid "%1 %2"
msgstr "%1 %2"

#: qml/OptionsPanel.qml:81
#, kde-format
msgid "Show More"
msgstr "Показване на още"

#: qml/OptionsPanel.qml:91
#, kde-format
msgid "Configure Visibility"
msgstr "Настройване на видимостта"

#: qml/OptionsPanel.qml:106
#, kde-format
msgid "Reselect Scanner"
msgstr "Повторно избиране на скенера"

#: qml/SettingsWindow.qml:20
#, kde-format
msgid "Configure"
msgstr "Настройване"

#: qml/SettingsWindow.qml:38
#, kde-format
msgid "Devices to show:"
msgstr "Устройства за показване:"

#: qml/SettingsWindow.qml:39
#, kde-format
msgctxt "@option:radio Devices to show for scanning"
msgid "Scanners only"
msgstr "Само скенери"

#: qml/SettingsWindow.qml:46
#, kde-format
msgctxt "@option:radio Devices to show for scanning"
msgid "Scanners, cameras, and virtual devices"
msgstr "Скенери, камери и виртуални устройства"

#: qml/SettingsWindow.qml:53
#, kde-format
msgid "Default file format:"
msgstr "Файлов формат по подразбиране:"

#: qml/SettingsWindow.qml:68
#, kde-format
msgid "Default save location:"
msgstr "Място за запис:"

#: qml/SettingsWindow.qml:101
#, kde-format
msgid "Close"
msgstr "Затваряне"

#: qml/ShareWindow.qml:42
#, kde-format
msgid "Share as:"
msgstr "Споделяне като:"
